package demo.abinar.app;

public class ProductoModel {
    private String nombre;
    private Integer cantidad;
    private String categoria;
    private String description;

    public ProductoModel(String nombre, Integer cantidad, String categoria, String description) {
        this.nombre = nombre;
        this.cantidad = cantidad;
        this.categoria = categoria;
        this.description = description;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
